# This is a dynv6 ddns client build with nodejs.

## How to set up ?
First you have to install nodejs. then Run CMD under the current directory use the following command: 

> npm install

After successful installation. you should set up the Configuration. there's a file named **cfg.json** in the **src** directory.
<br/>here's the format:

```
{
    "domain": "your domain",
    "token": "your token",
    "interval": 300000,     //update interval
    "error_retry_interval": 10000      //Error retry interval
}
```

Then all you have to do is run **ddnsClient.bat**.