import cfgType = require("./cfg.json");
import { utils } from "./utils";
import { ddns } from "./ddns";

function getCfg(): typeof cfgType {
    return require("./cfg.json");
}

async function process() {
    try {
        while (true) {
            console.clear();
            ddns.client.ins.domain = getCfg().domain;
            ddns.client.ins.token = getCfg().token;
            await ddns.client.ins.update();
            await utils.sleepPromise(getCfg().interval);
        }
    } catch (error) {
        console.log("错误： " + error + " ,正在重试.");
        await utils.sleepPromise(getCfg().error_retry_interval);
        process();
    }
}

process();

