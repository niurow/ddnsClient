import http = require("http");
import dns = require("dns");
import fs = require("fs");

export namespace utils {
    //等待
    export function sleepPromise(ms: number) {
        return new Promise(resolve => setTimeout(resolve, ms))
    }

    //http请求
    export function httpRequest(options: http.RequestOptions) {
        return new Promise<string>((resolve, reject) => {
            let req = http.request(options, (res) => {
                res.setEncoding("utf-8");
                res.on("data", function (chunk) {
                    resolve(chunk.toString())
                });
            });
            req.on("error", function (err) {
                reject(err.message);
            });
            req.end();
        })
    }

    //dns查询
    export function dnsLookup(domain: string) {
        return new Promise((resolve, reject) => {
            dns.lookup(domain, (err, address, family) => {
                if (err) {
                    reject(err.message);
                    return;
                }
                resolve(address);
            });
        })
    }

    //文件读取
    export function fsReadFileString(path: string) {
        return new Promise<string>((resolve, reject) => {
            fs.readFile(path, 'utf-8', (err, data) => {
                if (err) {
                    reject(err.message);
                    return;
                }
                resolve(data);
            })
        })
    }
}