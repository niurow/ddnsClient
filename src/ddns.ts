import { utils } from "./utils";

export namespace ddns {
    export class client {
        static ins: client = new client();
        domain: string = null;
        token: string = null;

        async update() {
            console.log("更新时间：" + new Date());
            let ipv6AddrJson = await utils.httpRequest({
                hostname: "api6.ipify.org",
                path: "/?format=json",
                family: 6,
            })
            let ipv6Addr = (<{ ip: string, }>JSON.parse(ipv6AddrJson)).ip;
            console.log("当前ipv6地址: " + ipv6Addr);
            if (ipv6Addr == null) {
                throw "ipv6地址获取失败";
            }

            let updateResult = await utils.httpRequest({
                hostname: "dynv6.com",
                path: `/api/update?hostname=${this.domain}&token=${this.token}&ipv6=${ipv6Addr}`,
            })
            console.log("更新结果：" + updateResult);

            let curDomainAddr = await utils.dnsLookup(this.domain);
            console.log("当前域名解析：" + curDomainAddr);
        }
    }
}